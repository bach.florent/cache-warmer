import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class dbConnector {

	public String dbhost;
	public int dbport;
	public String dbuser;
	public String dbpassword;
	public String dbname;
	
	public Connection conn;
	/** Constructor 
	 * @throws SQLException **/
	dbConnector(String host, int port, String user, String password, String name) throws SQLException{
		dbhost = host;
		dbport = port;
		dbuser = user;
		dbpassword = password;
		dbname = name;
		
		String myDriver = "com.mysql.cj.jdbc.Driver";
	    String myUrl = "jdbc:mysql://"+dbhost+":"+dbport+"/"+dbname;
	      try {
			Class.forName(myDriver);
			conn = DriverManager.getConnection(myUrl, dbuser, dbpassword);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void loadWebsites() throws SQLException {
		Statement st = conn.createStatement();
	}
	
	/* Execute Query */
	public void executeQuery(){
	}
}
