
public class website {

	int id;
	String name;
	String url;
	boolean enable;
	
	/** Constructor **/
	website (int websiteId, String websiteName, String urlSitemap, boolean warmerEnabled){
		id = websiteId;
		name = websiteName;
		url = urlSitemap;
		enable = warmerEnabled;
	}
}
