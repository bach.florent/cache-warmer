import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class main {

	static dbConnector connector;
	
	
	public static void main(String[] args) {
		
		System.out.println("Chargement des configurationns");
		// Chargement des configurations
		loadConfiguration();
		// Chargement des sites webs
		loadWebsite();
		
		while(true) {
			
		}
	}
	
	private static void loadConfiguration() {
		System.out.println("Loading env file");
		File file = new File("config.xml");
		
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();  
			DocumentBuilder db = dbf.newDocumentBuilder();  
			Document doc = db.parse(file);  
			doc.getDocumentElement().normalize();  
			NodeList nodeList = doc.getElementsByTagName("database");  
			
			
			for (int itr = 0; itr < nodeList.getLength(); itr++)   
			{  
				Node node = nodeList.item(itr);  

				if (node.getNodeType() == Node.ELEMENT_NODE)   
				{  
					Element eElement = (Element) node;  
					String dbhost = eElement.getElementsByTagName("host").item(0).getTextContent();
					int dbport = Integer.parseInt(eElement.getElementsByTagName("port").item(0).getTextContent());
					String dbuser = eElement.getElementsByTagName("user").item(0).getTextContent();
					String dbpassword =  eElement.getElementsByTagName("password").item(0).getTextContent(); 
					String dbname =  eElement.getElementsByTagName("dbname").item(0).getTextContent();
					
					connector = new dbConnector(dbhost, dbport, dbuser, dbpassword, dbname);
				}  
			}  
				
		}catch(Exception e) {
			System.out.println("Error : " + e);
		}
	}
	
	private static void loadWebsite() {
		
	}


}
